const express = require('express');
const serve   = require('express-static');

const app = express();

app.use(serve('/etc/www/public_html'));

const server = app.listen(80, function(){
  console.log('server is running at %s', server.address().port);
});
